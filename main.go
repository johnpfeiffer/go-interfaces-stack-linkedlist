package main

import (
	"errors"
	"fmt"
)

// TODO: main_test.go with many test data set variations, perf and benchmarks

// Stacker is a data structure that has specific data access and storage properties
// https://en.wikipedia.org/wiki/Stack_(abstract_data_type)
// https://golang.org/doc/effective_go.html#interfaces
// Interface function parameters
type Stacker interface {
	Size() int
	Show() []int
	Push(n int)
	Pop() (int, error)
}

// StackSum accepts a Stacker and returns the sum of all of the elements inside by emptying the stack
func StackSum(s Stacker) int {
	sum := 0
	initialSize := s.Size()
	for i := 0; i < initialSize; i++ {
		value, _ := s.Pop()
		sum += value
	}
	return sum
}

// DemoStack shows a demo example of different implementations of the interface
func DemoStack(s Stacker, name string) {
	s.Push(2)
	s.Push(1)
	s.Push(0)
	fmt.Println("\n", name, "has", s.Size(), "elements which are: ", s.Show())
	value, _ := s.Pop()
	fmt.Println("  pop a number off the stack:", value)
	fmt.Println("  The", s.Size(), "elements are: ", s.Show())
	fmt.Println("  The sum of all elements is:", StackSum(s))
	fmt.Println("  Since sum removes the elements size:", s.Size(), "elements = empty:", s.Show())
	_, err := s.Pop()
	fmt.Println("  Expected pop an empty stack error?", err)
}

// FakeStack fulfills the interface using only value receivers
type FakeStack struct {
	s []int
}

// Size returns 0
func (s FakeStack) Size() int {
	return 0
}

// Show returns an empty slice
func (s FakeStack) Show() []int {
	return []int{}
}

// Push nothing, the method receiver only uses a value so it could maybe use a global variable or another helper function?
func (s FakeStack) Push(n int) {

}

// Pop returns 0
func (s FakeStack) Pop() (int, error) {
	return 0, nil
}

/* A real stack based upon a slice */

// SliceStack is an integer stack data structure built using a slice
type SliceStack struct {
	s []int
}

// Size returns the total number of elements currently stored in the SliceStack
func (s SliceStack) Size() int {
	return len(s.s)
}

// Show displays the contents of the stack, pass a copy as no modification needed
func (s SliceStack) Show() []int {
	return s.s
}

// Push an integer onto the stack
// pass a reference so the receiver method can directly modify
// https://github.com/golang/go/wiki/CodeReviewComments#receiver-type
func (s *SliceStack) Push(n int) {
	s.s = append(s.s, n)
}

// Pop an integer off of the stack, removing it from the stack and returning the value
// pass a reference so the receiver method can modify
func (s *SliceStack) Pop() (int, error) {
	if len(s.s) <= 0 {
		return -1, errors.New("cannot pop an empty stack")
	}
	value := s.s[len(s.s)-1]
	s.s = s.s[0 : len(s.s)-1]
	return value, nil
}

/* A real stack based upon a linked list */

// IntNode is a pointer data structure for holding an integer
type IntNode struct {
	value int
	left  *IntNode
	right *IntNode
}

// LinkedListStack is an integer stack data structure built using a slice
type LinkedListStack struct {
	head *IntNode
	tail *IntNode
	size int
}

// Size returns the total number of elements currently stored in the LinkedListStack
func (s LinkedListStack) Size() int {
	return s.size
}

// Show returns the total number of elements currently stored in the LinkedListStack
func (s LinkedListStack) Show() []int {
	var result []int
	for current := s.head; current != nil; current = current.right {
		result = append(result, current.value)
	}
	return result
}

// Push an integer onto the stack, pass a reference so the receiver method can directly modify
func (s *LinkedListStack) Push(n int) {
	new := IntNode{value: n}
	s.size++
	if s.head == nil {
		s.head = &new
	} else {
		new.right = s.head
		s.head = &new
	}
}

// Pop an integer from the stack, pass a reference so the receiver method can directly modify
// return 0
func (s *LinkedListStack) Pop() (int, error) {
	if s.size <= 0 {
		return -1, errors.New("cannot pop an empty stack")
	}
	result := s.head.value
	s.head = s.head.right
	s.size--
	return result, nil
}

func main() {
	// a fake stack does not use pointer method receivers and must modify the data structure some other way
	var f FakeStack
	DemoStack(f, "FakeStack")

	// stacks that modify their underlying data structure must pass a reference
	var s SliceStack
	DemoStack(&s, "SliceStack")
	var k LinkedListStack
	DemoStack(&k, "LinkedListStack")
}
